cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject apache2-redirect-to-https.sh

    ds inject wireguard.sh
    ds inject www.sh
}
