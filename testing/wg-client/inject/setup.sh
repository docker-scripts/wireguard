#!/bin/bash -x

#source /host/settings.sh

# fix wg-quick
sed -i /usr/bin/wg-quick \
    -e '/net.ipv4.conf.all.src_valid_mark/d'

# create symbolic link to config file
ln -s /host/wg0.conf /etc/wireguard/
