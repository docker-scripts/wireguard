#!/bin/bash

cd $(dirname $0)
source settings.sh

main() {
    local cmd=$1
    case $cmd in
        up|down) $cmd  ;;
        *)       usage ;;
    esac
}

usage() {
    >&2 echo "Usage: $0 (up | down)"
    exit 1
}

up() {
    if [[ $ALLOW_INTERNET_ACCESS == 'yes' ]]; then
        iptables -I FORWARD -i wg0 -j ACCEPT
        iptables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
    fi

    if [[ $CLIENT_TO_CLIENT != 'yes' ]]; then
        iptables -I FORWARD -i wg0 -o wg0 -j DROP
    fi

    iptables -I INPUT -p udp -m udp --dport 51820 -j ACCEPT

    return 0
}

down() {
    if [[ $ALLOW_INTERNET_ACCESS == 'yes' ]]; then
        iptables -D FORWARD -i wg0 -j ACCEPT
        iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
    fi

    if [[ $CLIENT_TO_CLIENT != 'yes' ]]; then
        iptables -D FORWARD -i wg0 -o wg0 -j DROP
    fi

    iptables -D INPUT -p udp -m udp --dport 51820 -j ACCEPT

    return 0
}

main "$@"
