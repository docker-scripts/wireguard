#!/bin/bash

# customize settings.sh
public_ip=$(get_public_ip)
sed -i settings.sh \
    -e "/^PUBLIC_IP=/ c PUBLIC_IP='$public_ip'"
random_wg_port=$(shuf -i49152-65535 -n1)
sed -i settings.sh \
    -e "/^WG_PORT=/ c WG_PORT=$random_wg_port"
random_https_port="$(shuf -i11-45 -n1)443"
sed -i settings.sh \
    -e "/^HTTPS_PORT=/ c HTTPS_PORT=$random_https_port"

# copy wg0.sh
cp $SCRIPTS/wireguard/misc/wg0.sh .
chmod +x wg0.sh
