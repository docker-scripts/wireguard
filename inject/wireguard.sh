#!/bin/bash

# create the config file
[[ -f /host/wg0.conf ]] || cat <<EOF > /host/wg0.conf
[Interface]
Address = 10.100.100.1
ListenPort = 51820
PrivateKey = $(wg genkey)
PostUp = /host/wg0.sh up
PostDown = /host/wg0.sh down

EOF
chmod go-rwx wg0.conf
ln -s /host/wg0.conf /etc/wireguard/

# enable and start the service
systemctl enable wg-quick@wg0
systemctl start wg-quick@wg0
