include(bookworm)

RUN <<EOF
  # install wireguard
  apt install --yes iptables iproute2 wireguard

  # install apache2 and qrencode
  apt install --yes apache2 qrencode tree

  # install onionshare
  DEBIAN_FRONTEND=noninteractive \
      apt install --yes onionshare python3-requests

  systemctl disable tor apache2
EOF
