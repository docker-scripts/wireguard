APP=wireguard
NETWORK=wgnet
SUBNET='172.20.0.0/16'

### Public IP of the host.
### Usually it can be detected automatically.
### If not, make sure to set the correct value manually.
PUBLIC_IP='10.11.12.13'

### Forwarded ports
WG_PORT=51820
HTTPS_PORT=10443   # for sharing client config files

### Clients route these networks through the WG interface.
ROUTED_NETWORKS="0.0.0.0/0"

### Clients use these servers for resolving DNS queries.
### AdGuard DNS: https://adguard-dns.io/en/public-dns.html
DNS_SERVERS="94.140.14.14 94.140.15.15"

### Server provides internet access to clients through NAT.
ALLOW_INTERNET_ACCESS=yes

### Server allows clients to communicate with each-other.
#CLIENT_TO_CLIENT=yes

### Clients send a keep-connection-alive package periodically.
#KEEPALIVE_PERIOD=25
