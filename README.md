# WireGuard scripts

## Installation

- First install `ds`: https://gitlab.com/docker-scripts/ds#installation

- Then get the scripts: `ds pull wireguard`

- Create a directory for the container: `ds init wireguard @wireguard`

- Fix the settings: `cd /var/ds/wireguard/ ; vim settings.sh` <br/>
  Read also: [docs/wg-usecases.md](docs/wg-usecases.md)

- Make the container: `ds make`

## Usage

### On the server

1. Add a couple of clients:

   ```
   ds client add test1 192.168.10.2
   ds client ls
   ds client add test2 10.10.0.3
   ds client add test3 10.10.10.11
   ds client ls
   ls clients/
   ```

1. Delete one of them:

   ```
   ds client delete test1
   ds client ls
   ls clients/
   ```

1. Share a client config file:

   - Using QR code:
   
     ```
     ds share qr test2
     ```

     Scan it from Android or iPhone.

   - Using Tor:
   
     ```
     ds share tor test2
     ```

     Use a Tor Browser on the client side to download the
     configuration file.

   - Using www:
   
     ```
     ds share www test2
     ds share www test3
     ls www/
     ```

     On the client side use a command like this to get the config
     file:

     ```
     wget --no-check-certificate -O test3.conf \
	  https://11.12.13.14:4343/clients/test3.conf.HjamzWEpWW6z4LT
     ```

     To stop sharing by www run:

     ```
     ds share www stop
     ls www/
     ```

### On the client

1. Test the configuration file:

   ```
   apt install wireguard
   wg-quick up ./test2.conf
   
   ip addr
   ping 8.8.8.8
   traceroute 8.8.8.8
   curl ifconfig.me
   
   wg-quick down ./test2.conf
   ```

   **NB:** If your client is a RaspberryPi, you also have to install
   `raspberrypi-kernel-headers` and then reboot:

   ```
   apt install raspberrypi-kernel-headers
   reboot
   ```

1. Start the VPN connection as a service:

   ```
   mv test2.conf /etc/wireguard/wg0.conf
   systemctl enable wg-quick@wg0
   systemctl start wg-quick@wg0
   
   systemctl status wg-quick@wg0
   ip addr
   ping 8.8.8.8
   traceroute 8.8.8.8
   curl ifconfig.me
   ```

## Other commands

```
ds stop
ds start
ds shell
ds help
```
